const { checkQueueExists } = require('../lib/checkQueueExists')

const handler =  async (event, _context) => {
  const body = JSON.parse(event.body)
  const { placeId } = body

  try {
    const result = await checkQueueExists(placeId)
    const response = {
      queueLength: result.queueMembers.length,
      expectedDuration: 5*(result.queueMembers.length)
    }

    return {
      body: JSON.stringify({
        message: 'Ok',
        result: response
      }),
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      }
    }

  } catch (err) {
    console.error(err)

    return {
      body: JSON.stringify({
        error: err
      }),
      statusCode: 400
    }
  }
}

module.exports = { handler }
