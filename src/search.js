const { generateSearchResult } = require('../lib/generateSearchResult')

const handler =  async (_event, _context) => {

  try {
    const { results } = generateSearchResult()

    return {
      body: JSON.stringify({
        message: 'Ok',
        result: results
      }),
      statusCode: 200
    }

  } catch (err) {
    console.error(err)

    return {
      body: JSON.stringify({
        error: err
      }),
      statusCode: 400
    }
  }
}

module.exports = { handler }
