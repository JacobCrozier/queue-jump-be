const { checkQueueExists } = require('../lib/checkQueueExists')
const { startQueue } = require('../lib/startQueue')
const { addUserToQueue } = require('../lib/addUserToQueue')

const handler =  async (event, _context) => {
  const body = JSON.parse(event.body)
  const { placeId } = body

  try {
    const hasQueue = await checkQueueExists(placeId)
    console.log(hasQueue)
    let result
    if (!hasQueue.bool) {
      await startQueue(placeId)
      result = {
        queueLength: 1,
        expectedDuration: 5
      }
    } else {
      await addUserToQueue(hasQueue, placeId)
      result = {
        queueLength: hasQueue.queueMembers.length,
        expectedDuration: 5*(hasQueue.queueMembers.length)
      }
    }

    return {
      body: JSON.stringify({
        message: 'Ok',
        result: result
      }),
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      }
    }

  } catch (err) {
    console.error(err)

    return {
      body: JSON.stringify({
        error: err
      }),
      statusCode: 400
    }
  }
}

module.exports = { handler }
