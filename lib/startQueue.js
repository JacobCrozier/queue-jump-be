const aws = require('aws-sdk')
const { v1: uuid } = require('uuid')

const startQueue = async placeId => {
  const db = new aws.DynamoDB()

  const userId = uuid()

  let queue = [userId]

  const putItemParams = {
    TableName: process.env.QUEUE_TABLE,
    Item: {
      placeId: {
        S: placeId
      },
      queueMembers: {
        SS: queue
      },
    }
  }

  return db.putItem(putItemParams).promise()
}

module.exports = { startQueue }
