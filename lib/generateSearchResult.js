const generateSearchResult = () => {
  return {
    html_attributions: [ ],
    next_page_token: "CpQCAgEAAMFGl8zx5K_gYtgPHXejncejhOeu7hqyoTwzAHrmMCsKMyl4R0DNXljgu08nwzO3JRkZ1fwYkpgSy_qdCIjszj3hqAxaOUs6bcqlS2L8DEf45m7gt_zC4YDgcRF4HxwGgmCKX_EOKjfA5dfKMUW8ce4nTs1CoMeJxBjfsIZH5Xg4bAIDVty41o6KenHr55y7ZZiVHRDycNZj45Whd5zb84Je6Kn41Vk6LB2GiXlUhRiaX8AZ1Y9bl09Sz0oB8yYHkurmI4EF9LB4pbSb-H_UuC4oI4vjuesmliNRwqzYceAj3nNVZKR8vtzrmAWasLoXEEoppS6Av6ao4LuA9-rwXxtbaJrZCA2LOfISIKgxaOzDEhB_9Jom5TB1jrIHditAJCztGhQwpnhRxC0CxLAGXoCBtRCNH343UQ",
    results: [
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "12 Leamington Park, Acton, London W3 6TJ, United Kingdom",
    geometry: {
    location: {
    lat: 51.5202787,
    lng: -0.2634327
    },
    viewport: {
    northeast: {
    lat: 51.52155277989272,
    lng: -0.2620963701072777
    },
    southwest: {
    lat: 51.51885312010727,
    lng: -0.2647960298927222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "6968cc5dfba0259afb31619599a4b396ecd98605",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 3120,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/110351660754898902296">Ian Smith</a>`
    ],
    photo_reference: "CmRaAAAADHQYKVk-KOCTJ0BFSP85uQIMTXFhFdOI3M64PQ8k7yXYwtDZl-asRomNpAuY8xGnssjhneY6yfL-uTY8k1hCFK8Beuas82uhqvo84P6jUKDD-55KY2ngikOmQ0y22ufDEhAVBHqDkCbHNXpMEY7RZiFGGhSY8yq9N_NjffLk4cOyATSFb2faYQ",
    width: 4160
    }
    ],
    place_id: "ChIJPyfMSuERdkgR_PpeAVqSyYs",
    plus_code: {
    compound_code: "GPCP+4J London",
    global_code: "9C3XGPCP+4J"
    },
    price_level: 1,
    rating: 3.6,
    reference: "ChIJPyfMSuERdkgR_PpeAVqSyYs",
    types: [
    "meal_takeaway",
    "restaurant",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 1649
    },
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "48 Leicester Square, West End, London WC2H 7LU, United Kingdom",
    geometry: {
    location: {
    lat: 51.5101854,
    lng: -0.130869
    },
    viewport: {
    northeast: {
    lat: 51.51143562989272,
    lng: -0.1293983201072778
    },
    southwest: {
    lat: 51.50873597010727,
    lng: -0.1320979798927222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "775c912be0b3ac24fd06fa8ea0950dfddff611aa",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 3024,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/112984758408423473136">Veronika Jermolina</a>`
    ],
    photo_reference: "CmRaAAAAZgHU99OqqrUI-4F1u8M4d82VGhnvusVdtP5vwY_hjBhIbo5lTjxwrqtk_BYn0uQDip45tTNiS3jSk0aY6B3zjqdCY1jVN0HvYlmAaSASKVB4AMXnHlj0_GHO1kVmcH7HEhAW9kEP__7ErcrgEmdSaBOGGhRta-4R3W08o9XmNtTEQd_1FoRTZA",
    width: 4032
    }
    ],
    place_id: "ChIJk7WRItIEdkgR5Z_D0y6Pcko",
    plus_code: {
    compound_code: "GV69+3M London",
    global_code: "9C3XGV69+3M"
    },
    price_level: 1,
    rating: 3.8,
    reference: "ChIJk7WRItIEdkgR5Z_D0y6Pcko",
    types: [
    "restaurant",
    "meal_takeaway",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 2661
    },
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "Circus, 84-86 24 Charing Cross Road Cambridge, London WC2H 8AA, United Kingdom",
    geometry: {
    location: {
    lat: 51.5136763,
    lng: -0.129147
    },
    viewport: {
    northeast: {
    lat: 51.51500722989272,
    lng: -0.1278923201072778
    },
    southwest: {
    lat: 51.51230757010728,
    lng: -0.1305919798927222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "91ed35a84f30e3273ff9fe31bbefe5ec59a436a9",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 1139,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/106940218410473080523">fat les</a>`
    ],
    photo_reference: "CmRaAAAACWiDCw8lEOAYUCNtqfUdz3PSvtej_aNtShU4PMGToXyIx_r-7oZ4HlF7ZZ1cDALTheS9GskubVj3uwvB-08s3JZbruq7Z71rUBOBCwumRB7g9vQelYBSXKKl4dqFJxqBEhD93sRDwXZ2CopL4ByAF-MjGhRXJB0dMLrkEifYqcRN9bhdaOhoSA",
    width: 1719
    }
    ],
    place_id: "ChIJ9emzfs0EdkgRC7CzZ4A4qxk",
    plus_code: {
    compound_code: "GV7C+F8 London",
    global_code: "9C3XGV7C+F8"
    },
    price_level: 1,
    rating: 3.8,
    reference: "ChIJ9emzfs0EdkgRC7CzZ4A4qxk",
    types: [
    "restaurant",
    "meal_takeaway",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 1767
    },
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "178, 180 Edgware Rd, Marylebone, London W2 2DS, United Kingdom",
    geometry: {
    location: {
    lat: 51.51763649999999,
    lng: -0.1661418
    },
    viewport: {
    northeast: {
    lat: 51.51891072989272,
    lng: -0.1649298201072778
    },
    southwest: {
    lat: 51.51621107010727,
    lng: -0.1676294798927222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "09821f14170d189779a41f7a2ead76e6221a5135",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 2176,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/106276572486660370256">Alexandr Slaboussov</a>`
    ],
    photo_reference: "CmRaAAAAWC-HAuiA-eQkb48bR_52tsFi_nk569pe8bECuqK-qV3ga6mrP5Sm4efyuIjnTJIhfyRnOAhMJyAlflKphYELOOMYwsHNmV3kX7LIdBqiaAqCPgjuq7iKs-pEbUL4BhbZEhAwxTGGmrbCMYY0rkyUSH93GhQQ09jS0YhEO604XM-MVyuLPb9Wwg",
    width: 4608
    }
    ],
    place_id: "ChIJG4NToLUadkgRYLKItotkONg",
    plus_code: {
    compound_code: "GR9M+3G London",
    global_code: "9C3XGR9M+3G"
    },
    price_level: 1,
    rating: 3.7,
    reference: "ChIJG4NToLUadkgRYLKItotkONg",
    types: [
    "restaurant",
    "meal_takeaway",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 1468
    },
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "The Concourse, Liverpool Street Station, Unit 25 Liverpool St, London EC2M 7PY, United Kingdom",
    geometry: {
    location: {
    lat: 51.51925079999999,
    lng: -0.0810913
    },
    viewport: {
    northeast: {
    lat: 51.52036127989272,
    lng: -0.08029252010727779
    },
    southwest: {
    lat: 51.51766162010728,
    lng: -0.08299217989272223
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "888468ce2723e75f2289edbdea307ffb915e3c9b",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 722,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/110455273110811544949">Ben Kamal</a>`
    ],
    photo_reference: "CmRaAAAAMD2Ge0EGLuANIyjc_dj1Xuobec_yS3rANu1Ad3LjpEmjN9eVlV4FPnYR13xdwJkOpR7F2yfVPkkRKc4GIEewGgKUw3wQA_ObDhVaojwM_FhUWsk7xt1tSYeYONDVCcOYEhAWc7tw4kX4yYy3FJiCirVqGhTef8cn1TxKd1kzI9eanKw6ptnQig",
    width: 1080
    }
    ],
    place_id: "ChIJGS7Sga0cdkgR46nd-S661TM",
    plus_code: {
    compound_code: "GW99+PH London",
    global_code: "9C3XGW99+PH"
    },
    price_level: 1,
    rating: 3.5,
    reference: "ChIJGS7Sga0cdkgR46nd-S661TM",
    types: [
    "meal_takeaway",
    "restaurant",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 5085
    },
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "Quill St, Hanger Ln, Hanger Hill, London W5 1DN, United Kingdom",
    geometry: {
    location: {
    lat: 51.53386399999999,
    lng: -0.2960932
    },
    viewport: {
    northeast: {
    lat: 51.53510147989272,
    lng: -0.2949924701072778
    },
    southwest: {
    lat: 51.53240182010727,
    lng: -0.2976921298927223
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "916f1e6f2b30b5b19593034f0133072b4445453d",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 4160,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/116594953790864049522">Mariusz Brosty</a>`
    ],
    photo_reference: "CmRaAAAASPoW-bGKsslGVYGzs0A9J3ZAAp_Ag4T9oNdPRVC0C_K332h4eExtQ0Du2KprOpZ8UlhQ-a9zUNlpg3S-1q-mMexvxTRGi2Iz3IOgQjeG99uHU4cZ8shSejHUEGj2LFGhEhAZwC44DAfBf03GYfBgu8cdGhSSJ7VelHEmervw5CpJj4OjReUgmA",
    width: 3120
    }
    ],
    place_id: "ChIJ5UwZvBcSdkgRv9yZBWnfoaM",
    plus_code: {
    compound_code: "GPM3+GH London",
    global_code: "9C3XGPM3+GH"
    },
    price_level: 1,
    rating: 3.9,
    reference: "ChIJ5UwZvBcSdkgRv9yZBWnfoaM",
    types: [
    "restaurant",
    "meal_takeaway",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 2072
    },
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "50-52 The Broadway Units 1/3/22C The Arcadia Centre, London W5 2ND, United Kingdom",
    geometry: {
    location: {
    lat: 51.51350960000001,
    lng: -0.3041284
    },
    viewport: {
    northeast: {
    lat: 51.51488322989272,
    lng: -0.3028170201072777
    },
    southwest: {
    lat: 51.51218357010728,
    lng: -0.3055166798927222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "915c46cc401ad98d6b73877e65fb272cfd568b91",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 4032,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/110650688705020556200">Sumariati Gonzalez</a>`
    ],
    photo_reference: "CmRaAAAAqkqUu9Rvkbjeg9rUhHi2sXPZY_xUh88GRKRh3wct_czNTFWSHLr3P4xB1iFB5_RhmV8u3DIiJYG2PUhnjK5AYW3wRG4K45eUVPgLROlKzt6S5WKrUaWo32NPSZ06uayCEhBP__wlTYMdVVe2EDoz38lOGhTtk-K97U-pNl8f6kd2UkTrxat4Aw",
    width: 3024
    }
    ],
    place_id: "ChIJJfX8IEgNdkgR4AjEy6zgqoA",
    plus_code: {
    compound_code: "GM7W+C8 London",
    global_code: "9C3XGM7W+C8"
    },
    price_level: 1,
    rating: 3.7,
    reference: "ChIJJfX8IEgNdkgR4AjEy6zgqoA",
    types: [
    "restaurant",
    "meal_takeaway",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 985
    },
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "1a Ludgate Hill, London EC4M 7AA, United Kingdom",
    geometry: {
    location: {
    lat: 51.5136805,
    lng: -0.1008889
    },
    viewport: {
    northeast: {
    lat: 51.51508362989271,
    lng: -0.09952972010727777
    },
    southwest: {
    lat: 51.51238397010727,
    lng: -0.1022293798927222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "87f4cdae6f0acdc4edc3b1a8807f49ec6212ab1f",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 4032,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/114108028484803708133">Meow Meow</a>`
    ],
    photo_reference: "CmRaAAAAxc3dIumKw-9rbtW3YGBz2MjMz610shewq6cPE9KVWNnMy5f3TP1_tu48ADfgrb56-0n0hy_xq-zgyu9n3bvOANhLUXm5N4kWvVG3ulma33eZBbEVhfXPK5vlnrD46cpEEhBqSFGkAmAR3NABNOUUZ1jdGhSO1WRtyUaCY3_h42fSBv3qykLZiw",
    width: 3024
    }
    ],
    place_id: "ChIJedbqvawEdkgRteIsblwlQtQ",
    plus_code: {
    compound_code: "GV7X+FJ London",
    global_code: "9C3XGV7X+FJ"
    },
    price_level: 1,
    rating: 4.1,
    reference: "ChIJedbqvawEdkgRteIsblwlQtQ",
    types: [
    "restaurant",
    "meal_takeaway",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 1495
    },
    {
    business_status: "OPERATIONAL",
    formatted_address: "157 Colindeep Ln, London NW9 6DB, United Kingdom",
    geometry: {
    location: {
    lat: 51.5909597,
    lng: -0.2550274
    },
    viewport: {
    northeast: {
    lat: 51.59227027989273,
    lng: -0.2536250201072778
    },
    southwest: {
    lat: 51.58957062010728,
    lng: -0.2563246798927223
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "7b038a6f99fb9b35bb7ca4e4a43ed20c7c2c06d7",
    name: "McDonald's",
    opening_hours: {
    open_now: false
    },
    photos: [
    {
    height: 2322,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/102999774353811071452">Gee Baker</a>`
    ],
    photo_reference: "CmRaAAAALDFl1Yi5ABOfGborkoZvgKJZzAzOrhKJtVcsSBatPkqn-DLqfen7WWe9AVBg9Yh75YeVotD-tnXbEglIUqLow9VnjKuO_NAs37tRvnL0PB2tXYogkqPwZLQxPjcHekWQEhDNDpQrq1socjFBfJJnRD06GhSoR7fxz3eRfaAEUjVrsp3bUVVDWg",
    width: 4128
    }
    ],
    place_id: "ChIJAabxEa0adkgRnGu80a8cnus",
    plus_code: {
    compound_code: "HPRV+9X London",
    global_code: "9C3XHPRV+9X"
    },
    price_level: 1,
    rating: 3.8,
    reference: "ChIJAabxEa0adkgRnGu80a8cnus",
    types: [
    "restaurant",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 2582
    },
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "185-187 Oxford St, Soho, London W1D 2JU, United Kingdom",
    geometry: {
    location: {
    lat: 51.5154603,
    lng: -0.1388302
    },
    viewport: {
    northeast: {
    lat: 51.51691287989272,
    lng: -0.1371901701072778
    },
    southwest: {
    lat: 51.51421322010728,
    lng: -0.1398898298927222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "a963cbdd6f286cce069009915a8af5b4bd30b593",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 2268,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/117469094469133514801">Chris Stringer</a>`
    ],
    photo_reference: "CmRaAAAA8bckeHC3We8pq27FiLgdZ92vLsFZv-I7ax5pMSOWh8BMsnR1g4d4DHxjxNac-yX75OQc_XRWP6DWgALgUVpmPaGArb-bm66goh2zFCsoarf5PQFGmOXYEPf7-_KnT6NhEhCKAIUYn7zRbR1WNwM7EOg-GhSy1DB_k0GC8kZVfQAMHkDsP7d7KQ",
    width: 4032
    }
    ],
    place_id: "ChIJBd0NES0bdkgRcW--hZYnkP0",
    plus_code: {
    compound_code: "GV86+5F London",
    global_code: "9C3XGV86+5F"
    },
    price_level: 1,
    rating: 3.7,
    reference: "ChIJBd0NES0bdkgRcW--hZYnkP0",
    types: [
    "restaurant",
    "meal_takeaway",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 2161
    },
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "147, 151 High St, Acton, London W3 6LP, United Kingdom",
    geometry: {
    location: {
    lat: 51.5069087,
    lng: -0.2703978
    },
    viewport: {
    northeast: {
    lat: 51.50831542989271,
    lng: -0.2690063701072778
    },
    southwest: {
    lat: 51.50561577010727,
    lng: -0.2717060298927222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "e2b02915397b447e99089caac02bc0b676aa9622",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 7296,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/114647298085396208768">George Morina</a>`
    ],
    photo_reference: "CmRaAAAAJlVoGXqrpRBm_95m-4Q7KCRQVNWgpydtN3ZcPBwN2X4VvXFfNubVWUYaNZrPB8LZ065e8RM6yWIQ9vB0qMa7cMiqUwkqQjIJD9v4f1x1boFHS3oEL2rfpJiK--H3LHRREhBuR_c6dc8ZfkH3dq6mp5-MGhTZvmLW8IkXqKWTWvFtkIX_9X0vMg",
    width: 5472
    }
    ],
    place_id: "ChIJeQPmKhsOdkgR5gN1Km0zg2w",
    plus_code: {
    compound_code: "GP4H+QR London",
    global_code: "9C3XGP4H+QR"
    },
    price_level: 1,
    rating: 3.6,
    reference: "ChIJeQPmKhsOdkgR5gN1Km0zg2w",
    types: [
    "restaurant",
    "meal_takeaway",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 898
    },
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "139 N Circular Rd, Stonebridge, London NW10 0NX, United Kingdom",
    geometry: {
    location: {
    lat: 51.5516911,
    lng: -0.2616197
    },
    viewport: {
    northeast: {
    lat: 51.55301942989271,
    lng: -0.2602229201072777
    },
    southwest: {
    lat: 51.55031977010727,
    lng: -0.2629225798927222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "215ba4f95dae165edbc64e96b661848903d8ce6c",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 3024,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/101081098013046546852">Simeon Babadzhanov</a>`
    ],
    photo_reference: "CmRaAAAA6mUR7uJT5kaJSSDb2-x1q_B6qsR6wmgfWTIbGcFAihRfRGThkDeckPTDJ_JIem6zvOR095JWx4m_sZ1BzVmmD_CWYvTIJu8NFq3iIrRCHrtjNICt3ps0xbhSe7K_rxPvEhCeoJ2hnjmQAWzJmYgPgp5EGhT7Lazh_91yeH37Pc-ByZVUGzIx-Q",
    width: 4032
    }
    ],
    place_id: "ChIJ53vBNJkRdkgROSjJ1GjhmUI",
    plus_code: {
    compound_code: "HP2Q+M9 London",
    global_code: "9C3XHP2Q+M9"
    },
    price_level: 1,
    rating: 3.7,
    reference: "ChIJ53vBNJkRdkgROSjJ1GjhmUI",
    types: [
    "restaurant",
    "meal_takeaway",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 2171
    },
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "45/47 Whitehall, Westminster, London SW1A 2BX, United Kingdom",
    geometry: {
    location: {
    lat: 51.506311,
    lng: -0.1268678
    },
    viewport: {
    northeast: {
    lat: 51.50764757989272,
    lng: -0.1256318201072778
    },
    southwest: {
    lat: 51.50494792010727,
    lng: -0.1283314798927222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "a62fcc2b889b9237583c1959bbcc93d2409642c0",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 3264,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/105554190460897158302">Paul Atherton</a>`
    ],
    photo_reference: "CmRaAAAAngeLlaTsBdeRTshnCqY_R7DZAfI4VGajILs08g-jR3hNbucRe9no5I7VBPMNm0lYEjMVzmR__MYaEv8K-gTmj3UTrNrOq238zII08gIu79nzhbirGIqC_bkNHxWyaHJ9EhCoAZBuYmAaJ1VxTgb7JxuHGhSDILVUUgPT4Uc3Kn2ptFyTres-sQ",
    width: 2448
    }
    ],
    place_id: "ChIJXcy4sM0EdkgRHVCNscwi4vE",
    plus_code: {
    compound_code: "GV4F+G7 London",
    global_code: "9C3XGV4F+G7"
    },
    price_level: 1,
    rating: 3.5,
    reference: "ChIJXcy4sM0EdkgRHVCNscwi4vE",
    types: [
    "restaurant",
    "meal_takeaway",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 2448
    },
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "34/35 Strand, Charing Cross, London WC2N 5HY, United Kingdom",
    geometry: {
    location: {
    lat: 51.50888639999999,
    lng: -0.1244975
    },
    viewport: {
    northeast: {
    lat: 51.51026722989271,
    lng: -0.1232441201072778
    },
    southwest: {
    lat: 51.50756757010727,
    lng: -0.1259437798927222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "af3f25772988d73d8acdc9c0d7aa10ce892747b4",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 3120,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/106537502750902889758">Chris Petrie</a>`
    ],
    photo_reference: "CmRaAAAASZapDCohTlaVd6MkwE2IFpVMZXRwa2EMSd0Nc5cTuZH4rlAVNa9tMFdQmQglFNRUJ1c94Rx46vn0TK2Oftl-pMYqgUxuF2b4L2thtbVcxEgjWkMfEqsqQDKqErDzdYQxEhBlPJj03AQhZiXkF7MxJHm7GhRXyb0nh3pn52cRgGA92Q0Cx6wS3Q",
    width: 4160
    }
    ],
    place_id: "ChIJjeMSvM4EdkgRfqyISHLyD4Y",
    plus_code: {
    compound_code: "GV5G+H6 London",
    global_code: "9C3XGV5G+H6"
    },
    price_level: 1,
    rating: 3.7,
    reference: "ChIJjeMSvM4EdkgRfqyISHLyD4Y",
    types: [
    "meal_takeaway",
    "restaurant",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 4265
    },
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "108, 110 Notting Hill Gate, Notting Hill, London W11 3QA, United Kingdom",
    geometry: {
    location: {
    lat: 51.5090581,
    lng: -0.1980604
    },
    viewport: {
    northeast: {
    lat: 51.51030062989272,
    lng: -0.1966626701072778
    },
    southwest: {
    lat: 51.50760097010728,
    lng: -0.1993623298927222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "3866af81fe66660bf92aec58b0fadd5e10b1957a",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 3024,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/105522690074962315341">Navid Anayati</a>`
    ],
    photo_reference: "CmRaAAAAA96R5snJCKLtwrnxd2Dohcnq-9zz7PjbteKu2d8OTVCYJ_663i5lRriaV9iHRdk5kuKENINWfmfKZ5gzbKBex6QOtDE0Si84j-ikJhSY1JVY-8u9Q0HZaWPbMPz7usDoEhDO06WQFb25eCfwBm0yBzOOGhQDbmTXDhXUdSiiIMjQsf4ek0mHuQ",
    width: 4032
    }
    ],
    place_id: "ChIJJf8IePsPdkgREEj9AgFrnAs",
    plus_code: {
    compound_code: "GR52+JQ London",
    global_code: "9C3XGR52+JQ"
    },
    price_level: 1,
    rating: 3.7,
    reference: "ChIJJf8IePsPdkgREEj9AgFrnAs",
    types: [
    "restaurant",
    "meal_takeaway",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 1431
    },
    {
    business_status: "OPERATIONAL",
    formatted_address: "Sundial House, 108/110 Kensington High St, Kensington, London W8 4SG, United Kingdom",
    geometry: {
    location: {
    lat: 51.5012158,
    lng: -0.1931007
    },
    viewport: {
    northeast: {
    lat: 51.50250732989273,
    lng: -0.1916990201072778
    },
    southwest: {
    lat: 51.49980767010728,
    lng: -0.1943986798927222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "a3bdf1c79cfe1d1f9e27797b41e9690c3f8c2307",
    name: "McDonald's",
    opening_hours: {
    open_now: false
    },
    photos: [
    {
    height: 1500,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/106636974659226882269">A Google User</a>`
    ],
    photo_reference: "CmRaAAAAhPoL4UOTPkt2m2cYwk_7aSehDwj-tmBq9cK0NeLy0Ns592UA4UrHPpaggZe0tta_f-TuasJZqneveNwhO4XWQlVB-MVg6oLE2Of6LGK-Sj5gqd4uc5IkAve8KGNqpXdoEhDX_F1N7Lj0doKhwSKElfDhGhS-bi6N-ZLyrtlzk2aoADauZQXYEQ",
    width: 2000
    }
    ],
    place_id: "ChIJQdnj0PYPdkgRbbONsWhLmbM",
    plus_code: {
    compound_code: "GR24+FQ London",
    global_code: "9C3XGR24+FQ"
    },
    price_level: 1,
    rating: 3.7,
    reference: "ChIJQdnj0PYPdkgRbbONsWhLmbM",
    types: [
    "meal_takeaway",
    "restaurant",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 2046
    },
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "Tooley St, Bridge, London SE1 2TF, United Kingdom",
    geometry: {
    location: {
    lat: 51.5053302,
    lng: -0.0850643
    },
    viewport: {
    northeast: {
    lat: 51.50671657989272,
    lng: -0.08367342010727778
    },
    southwest: {
    lat: 51.50401692010728,
    lng: -0.08637307989272222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "a372ffdd20d13091865b05a9dd93d31cac0067ed",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 4032,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/108951040030285295212">Tim Scully</a>`
    ],
    photo_reference: "CmRaAAAAxq9Qt2qMkrLZjH2Cy1a9iuGk3-fjM0NSR6-17QerNondYQDGR3ZZIaEDuGFly2P92fBlZ6mJkK-1f-6249vettapGlYriLmDyWc99FqJeb2kYBuGj7Gg4KnMdT6aJaGHEhClqTBP5-TUZq0DE-WPO4MUGhRMBVIYjQl4mn657qYyuEKndcXvew",
    width: 3024
    }
    ],
    place_id: "ChIJwcW8EjIDdkgRV1USioNYUGU",
    plus_code: {
    compound_code: "GW47+4X London",
    global_code: "9C3XGW47+4X"
    },
    price_level: 1,
    rating: 3.5,
    reference: "ChIJwcW8EjIDdkgRV1USioNYUGU",
    types: [
    "restaurant",
    "meal_takeaway",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 802
    },
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "Leisure Node Southside, Wandsworth Shopping Centre, Garratt Ln, Wandsworth, London SW18 4TG, United Kingdom",
    geometry: {
    location: {
    lat: 51.45521,
    lng: -0.1939025
    },
    viewport: {
    northeast: {
    lat: 51.45621107989273,
    lng: -0.1924775201072778
    },
    southwest: {
    lat: 51.45351142010728,
    lng: -0.1951771798927222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "c156807fc15367b2e5b8b45177a2dc23d1c6d963",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 3024,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/110804793413226116018">Jordan Kirpalani</a>`
    ],
    photo_reference: "CmRaAAAA4BuB-Ui85v8ofNg_0CisUpNTl-I9naAZlZlDKPayl9SNiIXiLGEIcUdbjPbZmfGq3Ws_gd5yL20AnMZj0Id_pDp_2bbatSYbQK0Y7v4H6qUQAAQs4XuUefJllcSqYL9IEhBkiVxDhEJK5Bt2c4CLtNClGhSJPmQyMyIg5TuaGwIUAC0ghRbsGg",
    width: 4032
    }
    ],
    place_id: "ChIJ5_pr_sYPdkgR3h78xf3hEaQ",
    plus_code: {
    compound_code: "FR44+3C London",
    global_code: "9C3XFR44+3C"
    },
    price_level: 1,
    rating: 3.5,
    reference: "ChIJ5_pr_sYPdkgR3h78xf3hEaQ",
    types: [
    "restaurant",
    "meal_takeaway",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 392
    },
    {
    business_status: "CLOSED_TEMPORARILY",
    formatted_address: "Main Concourse, Wilton Rd Victoria Station, London SW1V 1JT, United Kingdom",
    geometry: {
    location: {
    lat: 51.4950079,
    lng: -0.143215
    },
    viewport: {
    northeast: {
    lat: 51.49646397989272,
    lng: -0.1416439201072778
    },
    southwest: {
    lat: 51.49376432010727,
    lng: -0.1443435798927222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "daa62a83d011998529a036909f3f5dc9aaa1d85b",
    name: "McDonald's",
    permanently_closed: true,
    photos: [
    {
    height: 4032,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/117781975984123235038">Naomi Madge</a>`
    ],
    photo_reference: "CmRaAAAAavBroE3_sz-nerztFMBpaQBNni0JsJU6md83CYPNmrvlW8PZ_W_WFYu89zw6K26gDIN9uIIO0NsdlMMeJ5Exi3qnV_J4XyD2UEyKUb9I8e2dbn7ua5E0ZpKbvcrMpt4WEhAKvrtxYTRqWtlLNJLtqU4VGhTufBh_1U36Yegtntp42KntXO1Evg",
    width: 3024
    }
    ],
    place_id: "ChIJ9_oLSiAFdkgR8aZ4zd1J9-s",
    plus_code: {
    compound_code: "FVW4+2P London",
    global_code: "9C3XFVW4+2P"
    },
    price_level: 1,
    rating: 3.5,
    reference: "ChIJ9_oLSiAFdkgR8aZ4zd1J9-s",
    types: [
    "restaurant",
    "meal_takeaway",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 387
    },
    {
    business_status: "OPERATIONAL",
    formatted_address: "Catford Island, Catford, London SE6 2DD, United Kingdom",
    geometry: {
    location: {
    lat: 51.4450326,
    lng: -0.0184625
    },
    viewport: {
    northeast: {
    lat: 51.44632062989272,
    lng: -0.01713542010727779
    },
    southwest: {
    lat: 51.44362097010728,
    lng: -0.01983507989272222
    }
    }
    },
    icon: "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
    id: "3ba5337c807e61d0496a618aafaae6c3121294f6",
    name: "McDonald's Catford 2",
    opening_hours: { },
    photos: [
    {
    height: 3024,
    html_attributions: [
    `<a href="https://maps.google.com/maps/contrib/117893929559434237574">A Google User</a>`
    ],
    photo_reference: "CmRaAAAAQjD4_oBD3mslYFBzZwLXepd5I19MTuzbpQvprG08MBiy0c6x_tbYU3dZHboeAdbHIEpp4lOeL9Kj5SxavjFvQ56lgCIHdPD6xFGNv1jAJeYy6hzfk4OXIQM1wdNdXz7pEhCwWCQFmBLp0Q0S593aCqk5GhSGpwreaJfZBBGz_7TyoHOmVIHyFg",
    width: 4032
    }
    ],
    place_id: "ChIJN2aXxhYCdkgRrA74yDZrBKQ",
    plus_code: {
    compound_code: "CXWJ+2J London",
    global_code: "9C3XCXWJ+2J"
    },
    price_level: 1,
    rating: 3.3,
    reference: "ChIJN2aXxhYCdkgRrA74yDZrBKQ",
    types: [
    "restaurant",
    "meal_takeaway",
    "food",
    "point_of_interest",
    "establishment"
    ],
    user_ratings_total: 1519
    }
    ],
    status: "OK"
    }
}

module.exports = {generateSearchResult}
