const aws = require('aws-sdk')

const checkQueueExists = async placeId => {
  const db = new aws.DynamoDB()

  const params = {
    Key: {
      placeId: {
        S: placeId
      }
    },
    TableName: process.env.QUEUE_TABLE
  }

  const result = await db.getItem(params).promise()
  if (result.Item === undefined) {
    return {
      bool: false,
      queueMembers: []
    }
  } else {
    return {
      bool: true,
      queueMembers: result.Item.queueMembers.SS
    }
  }
}

module.exports = { checkQueueExists }
