const aws = require('aws-sdk')
const { v1: uuid } = require('uuid')

const addUserToQueue = async (queue, placeId) => {
  const db = new aws.DynamoDB()
  const userId = uuid()
  const { queueMembers } = queue
  queueMembers.push(userId)

  const updateItemParams = {
    TableName: process.env.QUEUE_TABLE,
    ExpressionAttributeNames: {
      '#QM': 'queueMembers'
    },
    ExpressionAttributeValues: {
      ':qm': {
        SS: queueMembers
      },
    },
    Key: {
      placeId: {
        S: placeId
      }
    },
    ReturnValues: 'NONE',
    UpdateExpression: 'SET #QM = :qm'
  }

  return db.updateItem(updateItemParams).promise()
}

module.exports = { addUserToQueue }
