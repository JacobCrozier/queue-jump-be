const { handler } = require('../src/search')

jest.mock('../lib/generateSearchResult')
const { generateSearchResult } = require('../lib/generateSearchResult')

describe('search', () => {
  afterEach(() => {
    expect.hasAssertions()
  })

  test('resolves and returns correct response', async () => {
    generateSearchResult.mockImplementationOnce(() => {
      return {
        results: ['test', 'test', 'test']
      }
    })

    const expectedResult = {
      body: JSON.stringify({
        message: 'Ok',
        result:  ['test', 'test', 'test']
      }),
      statusCode: 200
    }

    const result = await handler()

    expect(result).toEqual(expectedResult)
  })
})
